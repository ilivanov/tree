package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strconv"
)

type myTree struct {
	parent *myTree
	data   string
	depth  int
	child  []*myTree
}

func (res *myTree) create(val string) {
	res.parent = nil
	res.data = val
	res.depth = 0
}

func (res *myTree) add(child *myTree) {
	res.child = append(res.child, child)
	child.parent = res
	child.depth = res.depth + 1
}

func getPathNode(node *myTree) []*myTree {
	var sl []*myTree
	for node.parent != nil {
		sl = append(sl, node)
		node = node.parent
	}
	return sl
}

func (child *myTree) chekIsChildeRoot() bool {
	if child.parent.parent == nil {
		return true
	} else {
		return false
	}
}

func (node *myTree) isLastChild() bool {
	if node.parent.child[len(node.parent.child)-1].data != node.data {
		return true
	} else {
		return false
	}

}

func (res *myTree) print(output io.Writer) {
	for i, ch := range res.child {
		temp := getPathNode(ch)
		for i := len(temp) - 1; i > 0; i-- {
			if temp[i].chekIsChildeRoot() && temp[i].data != ch.data && temp[i].isLastChild() {
				fmt.Fprint(output, "|")
			} else {
				if temp[i].isLastChild() {
					fmt.Fprint(output, "|")
				}
			}
			fmt.Fprint(output, "	")
		}

		if len(res.child)-1 == i {
			fmt.Fprint(output, "└───")
		} else {
			fmt.Fprint(output, "├───")
		}
		fmt.Fprint(output, ch.data)
		fmt.Fprintln(output)
		if len(ch.child) != 0 {
			ch.print(output)
		}
	}
}

func isValueInList(value string, list []string) bool {
	for _, v := range list {
		if v == value {
			return true
		}
	}
	return false
}

func dirTree(output io.Writer, s string, b bool) error {
	var res = myTree{}
	res.create("root")
	crTree(&res, s, b)
	res.print(output)
	return nil
}

func crTree(res *myTree, s string, b bool) error {
	files, err := ioutil.ReadDir(s)
	if err != nil {
		log.Fatal(err)
	}

	sort.Slice(files, func(i, j int) bool { return files[i].Name() < files[j].Name() })

	for _, file := range files {
		ifFolderTrue := file.IsDir()
		child := myTree{}

		if !b && ifFolderTrue {
			child.create(file.Name())
			res.add(&child)
		}

		if b {
			var itog string
			if ifFolderTrue {
				itog = file.Name()
			} else {
				var size int64
				if size = file.Size(); size == 0 {
					itog = "empty"
				} else {
					itog = strconv.FormatInt(file.Size(), 10) + "b"
				}
				itog = file.Name() + " (" + itog + ")"
			}
			child.create(itog)
			res.add(&child)
		}

		if ifFolderTrue {
			err1 := crTree(&child, s+`\`+file.Name(), b)
			if err1 != nil {
				panic(err1.Error())
			}
		}
	}
	return nil
}

func uniq(output io.Writer) error {

	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"

	pwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	err = dirTree(output, pwd, printFiles)
	if err != nil {
		panic(err.Error())
	}
	return nil
}

func main() {
	err := uniq(os.Stdout)
	if err != nil {
		panic(err.Error())
	}
}
